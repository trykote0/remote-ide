FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y \
    openjdk-17-jdk \
    maven gradle \
    wget curl \
    bash htop \
    ssh iproute2 \
    git mercurial subversion \
    awscli \
    apt-transport-https ca-certificates \
    gnupg lsb-release \
    iputils-ping traceroute dnsutils \
    && rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && \
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt-get update && \
    apt-get install -y docker-ce docker-ce-cli containerd.io supervisor openvpn \
    && rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64

RUN mkdir /var/run/sshd \
    && echo 'PasswordAuthentication no' >> /etc/ssh/sshd_config \
    && echo 'PermitRootLogin prohibit-password' >> /etc/ssh/sshd_config

COPY keys/id_rsa.pub /root/.ssh/authorized_keys
COPY vcs_keys/ /root/.ssh/
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY scripts/start_openvpn.sh /root/start_openvpn.sh

RUN chmod +x /root/start_openvpn.sh
RUN chmod 700 /root/.ssh \
    && chmod 600 /root/.ssh/authorized_keys

EXPOSE 22

RUN mkdir /projects
RUN mkdir /vpn

VOLUME /projects
VOLUME /root/.cache
VOLUME /root/.config
VOLUME /root/.aws
VOLUME /root/.gradle
VOLUME /root/.m2

COPY aws/ /root/.aws/
COPY openvpn/ /vpn/

CMD bash -c "/sbin/ip addr && /usr/bin/supervisord"