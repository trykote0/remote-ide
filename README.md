# Dockerfile Documentation

This document provides an overview of the Dockerfile which is designed to set up a development environment with various tools and services installed.

## Base Image

- **Base Image**: `debian:bullseye-slim`

## Installed Packages

The following packages are installed in the image:

- **Java Development Kit (JDK)**: `openjdk-17-jdk`
- **Build Tools**: `maven`, `gradle`
- **Networking Tools**: `wget`, `curl`, `ssh`, `iproute2`, `iputils-ping`, `traceroute`, `dnsutils`
- **Shell and System Tools**: `bash`, `htop`
- **Version Control Systems**: `git`, `mercurial`, `subversion`
- **AWS CLI**: `awscli`
- **Security and Certificates**: `apt-transport-https`, `ca-certificates`, `gnupg`, `lsb-release`

## Environment Variables

- **JAVA_HOME**: Set to `/usr/lib/jvm/java-17-openjdk-amd64`

## SSH Configuration

- SSHD is configured to disallow password authentication and root login without password.
- SSH keys are copied to the container for authentication.

## Data Copying

- AWS Credentials: Copied from `aws/` to `/root/.aws/`.
- OpenVPN Configurations: Copied from `openvpn/` to `/vpn/`.
- SSH Credentials:
    - Public Key: Copied from `keys/id_rsa.pub` to `/root/.ssh/authorized_keys`.
    - Version Control System Keys: Copied from `vcs_keys/` to `/root/.ssh/`.


## Supervisor Configuration

The `supervisord.conf` file configures supervisor to manage services within the container. It sets up an HTTP server for supervisor on port 9001, runs supervisor in the foreground as root, and defines several programs including SSHD, Docker daemon, OpenVPN, and a command to display IP address information. Each program has its own configuration regarding how it should be executed and managed.

## Directories and Volumes

- **Directories Created**:
    - `/var/run/sshd`: For SSH daemon.
    - `/projects`: General-purpose directory for projects.
    - `/vpn`: For VPN configurations.

- **Volumes Defined**:
    - `/projects`: To persist project data across container restarts.
    - `/root/.cache`: To persist cache data.
    - `/root/.config`: To persist configuration data.
    - `/root/.aws`: To persist AWS CLI configurations and credentials.
    - `/root/.gradle`: To persist Gradle cache and configurations.
    - `/root/.m2`: To persist Maven cache and configurations.

## Entry Point

- The container entry point is set to execute a script that outputs IP address information and starts supervisord.
