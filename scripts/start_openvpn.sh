#!/bin/bash

for file in /vpn/*.ovpn; do
  echo "Starting OpenVPN with config: $file"
  /usr/sbin/openvpn $file &
done
tail -f /dev/null
